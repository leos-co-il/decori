<?php
/*
Template Name: אודות
*/

the_post();
get_header();
$fields = get_fields();
$post_img = has_post_thumbnail() ? postThumb() : '';
?>
<article class="page-body blog-body mb-5">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container pb-5">
		<div class="row justify-content-between align-items-center">
			<div class="<?= $post_img ? 'col-lg-6 col-12' : 'col-12'; ?>">
				<div class="base-output">
					<h1><?php the_title(); ?></h1>
					<?php the_content(); ?>
				</div>
			</div>
			<?php if ($post_img) : ?>
				<div class="col-xl-5 col-lg-6 col-12 slider-img-col">
					<div class="slider-img-wrap">
						<img src="<?= $post_img; ?>" alt="image">
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'benefits', [
	'title' => $fields['about_benefits_title'],
	'benefits' => $fields['about_benefits']
]);
if ($fields['about_content']) : ?>
<section class="content-block">
	<?php foreach ($fields['about_content'] as $content) : if (isset($content['text']) && $content['text']) :
		$img = (isset($content['img']) && $content['img'] ? $content['img'] : ''); ?>
		<div class="container content-block-container">
			<div class="row justify-content-between align-items-center row-content-block">
				<?php if ($img) : ?>
					<div class="col-xl-5 col-lg-6 col-12 slider-img-col">
						<div class="slider-img-wrap">
							<img src="<?= $img['url']; ?>" alt="image">
						</div>
					</div>
				<?php endif; ?>
				<div class="<?= $img ? 'col-lg-6 col-12' : 'col-12'; ?>">
					<div class="base-output">
						<?= $content['text']; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; endforeach; ?>
</section>
<?php endif; ?>
<section class="about-reviews">
	<?php get_template_part('views/partials/repeat', 'reviews'); ?>
</section>
<?php if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>

