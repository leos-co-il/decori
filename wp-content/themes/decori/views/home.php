<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>
<section class="home-main-block">
	<div class="main-block-home">
		<div class="container">
			<?php if ($logo = opt('logo_white')) : ?>
				<div class="row justify-content-center">
					<div class="col-sm-6 col-8">
						<a class="logo-home" href="/">
							<img src="<?= $logo['url']; ?>" alt="decori">
						</a>
					</div>
				</div>
			<?php endif;
			if ($fields['h_main_title'] || $fields['h_main_link']) : ?>
				<div class="row justify-content-center">
					<div class="col-12">
						<h2 class="homepage-main-title">
							<?= $fields['h_main_title']; ?>
						</h2>
						<?php if ($fields['h_main_link']) : ?>
							<div class="row justify-content-center">
								<div class="col-auto">
									<a href="<?= isset($fields['h_main_link']['url']) ? $fields['h_main_link']['url'] : ''; ?>" class="main-link">
										<?= isset($fields['h_main_link']['title']) ? $fields['h_main_link']['title'] : esc_html__('התחילו לחסוך', 'leos'); ?>
									</a>
								</div>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if ($fields['main_slider']) : ?>
		<div class="main-slider" dir="rtl">
			<?php foreach ($fields['main_slider'] as $slide) : ?>
				<?php $img = ''; $video = '';
				if (isset($slide['main_back']['0'])) {
					if ($slide['main_back']['0']['acf_fc_layout'] === 'main_back_img') {
						$img = isset($slide['main_back']['0']['img']) ? $slide['main_back']['0']['img'] : '';
					} elseif ($slide['main_back']['0']['acf_fc_layout'] === 'main_back_video') {
						$video = isset($slide['main_back']['0']['video']) ? $slide['main_back']['0']['video'] : '';
					}
				}
				if ($video) : ?>
					<div class="slide-video">
						<video muted autoplay="autoplay" loop="loop">
							<source src="<?= $video['url']; ?>" type="video/mp4">
						</video>
					</div>
				<?php else: ?>
					<div class="slide-main" style="background-image: url('<?= $img ? $img['url'] : ''; ?>')">
					</div>
				<?php endif;
			endforeach; ?>
		</div>
	<?php endif; ?>
</section>
<?php get_template_part('views/partials/content', 'slider_products', [
		'title' => $fields['h_products_title'],
		'products' => $fields['h_products'],
]);
if ($fields['h_categories']) : $chunks = array_chunk($fields['h_categories'], 6);?>
	<div class="home-categories-block arrows-slider arrows-slider-bottom">
		<div class="base-slider" dir="rtl">
			<?php foreach ($chunks as $array_cats) : ?>
				<div>
					<div class="container-categories">
						<?php foreach ($array_cats as $x => $cat) : ?>
							<a class="category-col category-col-<?= $x + 1; ?>" href="<?= get_term_link($cat); ?>">
								<?php $thumbnail_id = get_term_meta( $cat->term_id, 'thumbnail_id', true );
								$image = wp_get_attachment_url( $thumbnail_id );
								if ($image) : ?>
									<img src="<?= $image; ?>" alt="image">
								<?php endif; ?>
								<span class="category-name"><?= $cat->name; ?></span>
							</a>
						<?php endforeach; ?>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
<?php endif;
get_template_part('views/partials/content', 'slider_products', [
		'title' => $fields['h_products_title_2'],
		'products' => $fields['h_products_2'],
]);
if ($fields['h_banners']) : ?>
	<section class="banners-block">
		<div class="container">
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($fields['h_banners'] as $banner) : $b_img = 0; ?>
					<div class="col-lg-6 col-12 banner-col">
						<div class="row justify-content-center align-items-stretch row-banner-item">
							<?php if (isset($banner['img']) && $banner['img'] && isset($banner['img']['url'])) : $b_img = $banner['img']['url']; ?>
								<div class="col-xl-5 col-lg-4 col-12 banner-img" style="background-image: url('<?= $b_img; ?>')"></div>
							<?php endif; ?>
							<div class="<?= $b_img ? 'col-xl-7 col-lg-8 col-12' : 'col-12'; ?>">
								<div class="banner-content">
									<h2 class="banner-title"><?= $banner['title']; ?></h2>
									<h3 class="banner-subtitle"><?= $banner['subtitle']; ?></h3>
									<?php if ($banner['link'] && isset($banner['link']['url'])) : ?>
										<a href="<?= $banner['link']['url']; ?>" class="main-link">
											<?= isset($banner['link']['title']) && $banner['link']['title'] ? $banner['link']['title'] :
													esc_html__('התחילו לקנות עכשיו', 'leos'); ?>
										</a>
									<?php endif; ?>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif;
get_template_part('views/partials/repeat', 'reviews');
get_template_part('views/partials/content', 'slider_products', [
		'title' => $fields['h_products_sale_title'],
		'products' => $fields['h_products_sale'],
]);
get_footer(); ?>
