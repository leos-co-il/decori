<?php if(isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-col">
		<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
			<a class="post-inside-image" href="<?= $link; ?>">
				<?php if (has_post_thumbnail($args['post'])) : ?>
					<img src="<?= postThumb($args['post']); ?>" alt="post-image">
				<?php endif; ?>
				<span class="post-card-title-wrap">
					<h3 class="post-card-title"><?= $args['post']->post_title; ?></h3>
				</span>
			</a>
			<div class="post-item-content">
				<p class="base-text text-center">
					<?= text_preview($args['post']->post_content, 15); ?>
				</p>
			</div>
			<a href="<?= $link; ?>" class="post-link">
				המשך קריאה >
			</a>
		</div>
	</div>
<?php endif; ?>
