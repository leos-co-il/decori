<?php
$benefits = (isset($args['benefits']) && $args['benefits']) ? $args['benefits'] : opt('benefits');
$title = (isset($args['title']) && $args['title']) ? $args['title'] : opt('benefits_title');
if ($benefits) : ?>
	<div class="benefits-block">
		<div class="container benefits-container">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<?php if ($title) : ?>
						<div class="row justify-content-center">
							<div class="col-auto">
								<h2 class="block-title"><?= $title; ?></h2>
							</div>
						</div>
					<?php endif; ?>
					<div class="row justify-content-center align-items-stretch">
						<?php foreach ($benefits as $x => $why_item) : ?>
							<div class="col-md-4 col-sm-6 col-12 why-item-col wow fadeInUp" data-wow-delay="0.<?= ($x + 1) * 2; ?>s">
								<div class="why-item">
									<div class="why-icon-wrap">
										<?php if ($why_item['icon']) : ?>
											<img src="<?= $why_item['icon']['url']; ?>" alt="benefit-icon"
												 class="wow pulse" data-wow-delay="0.<?= ($x + 1) * 2; ?>s">
										<?php endif; ?>
									</div>
									<h4 class="why-item-title">
										<?= $why_item['title']; ?>
									</h4>
									<p class="why-item-text">
										<?= $why_item['subtitle']; ?>
									</p>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php endif; ?>
