<?php
$address = opt('address');
$facebook = opt('facebook');
$youtube = opt('youtube');
$instagram = opt('instagram');
$whatsapp = opt('whatsapp');
?>
<div class="socials-line">
	<?php if ($instagram) : ?>
		<a href="<?= $instagram; ?>" class="social-link-item">
			<img src="<?= ICONS ?>instagram.png" alt="instagram-icon">
		</a>
	<?php endif;
	if ($youtube) : ?>
		<a href="<?= $youtube; ?>" class="social-link-item">
			<img src="<?= ICONS ?>youtube.png" alt="youtube-icon">
		</a>
	<?php endif;
	if ($facebook) : ?>
		<a href="<?= $facebook; ?>" class="social-link-item">
			<img src="<?= ICONS ?>facebook.png" alt="facebook-icon">
		</a>
	<?php endif;
	if ($address) : ?>
		<a href="https://waze.com/ul?q=<?= $address; ?>"
		   class="social-link-item" target="_blank">
			<img src="<?= ICONS ?>waze.png" alt="waze-icon">
		</a>
	<?php endif;
	if ($whatsapp) : ?>
		<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="social-link-item">
			<img src="<?= ICONS ?>whatsapp.png" alt="whatsapp">
		</a>
	<?php endif; ?>
</div>
