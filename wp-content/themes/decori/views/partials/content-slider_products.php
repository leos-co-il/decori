<?php

if(!isset($args['products']))
	return;
$title = (isset($args['title']) && $args['title']) ? $args['title'] : '';
?>
<section class="products-slider-output arrows-slider">
	<div class="container">
		<?php if ($title) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="block-title"><?= $title; ?></h2>
				</div>
			</div>
		<?php endif; ?>
		<div class="row">
			<div class="col-12">
				<div class="product-slider" dir="rtl">
					<?php

					foreach ($args['products'] as $_p){
						echo '<div class="slide-item h-100 px-2">';
						setup_postdata($GLOBALS['post'] =& $_p);
						wc_get_template_part( 'content', 'product' );
						echo '</div>';
					}
					wp_reset_postdata();

					?>
				</div>
			</div>
		</div>
	</div>
</section>
