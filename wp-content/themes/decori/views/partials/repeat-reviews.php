<?php $reviews = opt('reviews');
$title = opt('reviews_title');
$back = opt('reviews_back');
if ($reviews) : ?>
<section class="reviews-block arrows-slider" <?php if (isset($back['url']) && $back['url']) : ?>
	style="background-image: url('<?= $back['url']; ?>')"
<?php endif; ?>>
	<span class="benefits-block-overlay"></span>
	<div class="container benefits-container">
		<?php if ($title) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="block-title">
						<?= $title; ?>
					</h2>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center align-items-center">
			<div class="col-12">
				<div class="slider-content-wrap">
					<div class="reviews-slider" dir="rtl">
						<?php foreach ($reviews as $x => $review) : ?>
							<div class="px-2 mb-relative">
								<div class="review-slide">
									<div class="rev-content">
										<h3 class="review-title"><?= $review['name']; ?></h3>
										<div class="base-output text-center">
											<?= $review['text']; ?>
										</div>
										<?php if (isset($review['link']) && $review['link']) : ?>
											<a href="<?= $review['link']; ?>" class="review-link">
												המשך קריאה >
											</a>
										<?php endif; ?>
									</div>
									<?php if (isset($review['img']) && $review['img']) : ?>
										<div class="review-image">
											<img src="<?= $review['img']['url']; ?>" alt="author-of-review">
										</div>
									<?php endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
