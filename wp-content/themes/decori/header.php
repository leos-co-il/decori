<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif;
$tel = opt('tel'); ?>
<header class="sticky">
	<div class="drop-container">
	</div>
	<?php if($flash_msg = opt('news')): ?>
		<div class="flash-msg-wrap">
			<div class="container-fluid">
				<div class="row justify-content-center">
					<div class="col-xl-11">
						<div class="row justify-content-center align-items-center">
							<div class="col-md col-auto"></div>
							<div class="col-md-6 col-11">
								<div class="flash-msg-slider" dir="rtl">
									<?php foreach ($flash_msg as $msg): ?>
										<div class="single-msg">
											<?php if($msg['link']): ?>
											<a href="<?= $msg['link'] ?>" title="<?= $msg['title'] ?>">
												<?php endif; ?>
												<span class="msg-text"><?= $msg['title'] ?></span>
												<?php if($msg['link']): ?>
											</a>
										<?php endif; ?>
										</div>
									<?php endforeach; ?>
								</div>
							</div>
							<div class="col-md col-auto tel-col">
								<?php if ($tel) : ?>
									<img src="<?= ICONS ?>header-tel.png" alt="phone">
									<a href="tel:<?= $tel; ?>" class="header-tel">
										<span class="tel-number"><?= $tel; ?></span>
									</a>
								<?php endif;
								if ($whatsapp = opt('whatsapp')) : ?>
									<a href="https://api.whatsapp.com/send?phone=<?= $whatsapp; ?>" class="header-tel">
										<?= '|  '.$whatsapp; ?>
									</a>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="header-top">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-xl-11">
					<div class="row align-items-center justify-content-md-between justify-content-center">
						<div class="col d-flex align-items-center justify-content-start col-hidden">
						</div>
						<?php if ($logo = opt('logo')) : ?>
							<div class="col-xl-2 col-lg-3 col-md-4 col-sm-auto col-6 logo-col-header">
								<a href="/" class="logo">
									<img src="<?= $logo['url'] ?>" alt="logo">
								</a>
							</div>
						<?php endif; ?>
						<div class="col-md col-12 d-flex justify-content-end align-items-center">
							<a class="log-button header-btn" href="<?= is_user_logged_in() ? get_permalink(get_option('woocommerce_myaccount_page_id')) : wp_registration_url(); ?>">
								<?= svg_simple(THEMEPATH . '/assets/iconsall/user.svg') ?>
							</a>
							<div class="search-trigger position-relative header-btn">
								<?= svg_simple(ICONS.'search.svg'); ?>
							</div>
							<?php if(defined( 'YITH_WCWL' )): ?>
								<a href="<?= get_permalink(get_option('yith_wcwl_wishlist_page_id')); ?>" class="header-btn">
									<?= svg_simple(THEMEPATH . '/assets/iconsall/wishlist.svg') ?>
									<span class="count-circle" id="yith-w-count">
							<?= esc_html(yith_wcwl_count_all_products()) ?>
						</span>
								</a>
							<?php endif; ?>
							<a href="#" class="header-btn" id="mini-cart">
								<?= svg_simple(THEMEPATH . '/assets/iconsall/basket.svg') ?>
								<span class="count-circle" id="cart-count">
						<?= WC()->cart->get_cart_contents_count(); ?>
					</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header-bottom">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col">
					<nav id="desktopNav" class="h-100 ml-xl-3 ml-0">
						<?php getMenu('header-menu', '2', '', 'h-100'); ?>
					</nav>
					<nav id="MainNav" class="h-100 ml-xl-3 ml-0">
						<div id="MobNavBtn">
							<span></span>
							<span></span>
							<span></span>
						</div>
						<?php getMenu('header-menu', '2', '', 'main_menu h-100'); ?>
					</nav>
				</div>
			</div>
		</div>
	</div>
</header>

<div class="mini-cart-wrap" id="mini-cart-el">
	<?php wc_get_template( 'cart/mini-cart.php' ); ?>
</div>

<div class="triggers-fix">
	<?php get_template_part('views/partials/repeat', 'socials'); ?>
	<div class="pop-trigger">
		<img src="<?= ICONS ?>pop-trigger.png" alt="contact-us">
	</div>
</div>
<div class="pop-great"></div>
<div class="float-form">
	<div class="pop-form">
		<div class="contact-form-wrap form-wrapper">
			<?php if ($title = opt('pop_form_title')) : ?>
				<h2 class="form-title">
					<?= $title; ?>
				</h2>
			<?php endif;
			getForm('14'); ?>
		</div>
	</div>
</div>
<div class="float-search">
	<span class="close-search">
		<img src="<?= ICONS ?>cancel.png">
	</span>
	<?php get_search_form(); ?>
</div>
<?php if ($tel) : ?>
<a class="tel-col-mobile" href="tel:<?= $tel; ?>">
	<img src="<?= ICONS ?>header-tel.png" alt="phone">
</a>
<?php endif; ?>
