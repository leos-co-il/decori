<form role="search" method="get" class="search-form" action="">
	<input id="search-input" type="search" class="search-input" placeholder="<?= esc_html__('חיפוש','leos') ?>" value="" name="s" title="<?= esc_html__('חפש באתר','leos') ?>" />
	<input type="submit" class="search-submit" value="" />
</form>
