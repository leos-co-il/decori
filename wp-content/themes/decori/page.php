<?php

the_post();
get_header();
$fields = get_fields();

?>
<article class="page-body blog-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="base-output">
					<?php the_content(); ?>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
