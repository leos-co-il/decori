<?php
get_header();
$fields = get_fields();
?>
<div class="post-output-block mb-5">
	<div class="container">
		<?php
		$s = get_search_query();
		$args_1 = array(
				'post_type' => 'post',
				's' => $s
		);
		$args_2 = array(
				'post_type' => 'product',
				's' => $s
		);
		$the_query_1 = new WP_Query( $args_1 );
		$the_query_2 = new WP_Query( $args_2 );
		if ( $the_query_1->have_posts() ) { ?>
		<h4 class="block-title"><?= esc_html__('תוצאות חיפוש עבור:','leos');?><?= get_query_var('s') ?></h4>
		<div class="row justify-content-center align-items-stretch">
			<?php while ( $the_query_1->have_posts() ) { $the_query_1->the_post();
				$link = get_the_permalink();
				$base_img = has_post_thumbnail() ? postThumb() : '';
				?>
				<div class="col-xl-3 col-lg-4 col-sm-6 col-12 post-col">
					<div class="post-card more-card">
						<a class="post-inside-image" href="<?= $link; ?>">
							<?php if ($base_img) : ?>
								<img src="<?= $base_img; ?>" alt="post-image">
							<?php endif; ?>
							<span class="post-card-title-wrap">
					<h3 class="post-card-title"><?php the_title(); ?></h3>
				</span>
						</a>
						<div class="post-item-content">
							<p class="base-text text-center">
								<?= text_preview(get_the_content(), 15); ?>
							</p>
						</div>
						<a href="<?= $link; ?>" class="post-link">
							המשך קריאה >
						</a>
					</div>
				</div>
			<?php }
			} else{ ?>
				<div class="col-12 pt-5">
					<h4 class="block-title">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h4>
				</div>
				<div class="alert alert-info text-center mt-5">
					<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
				</div>
			<?php } ?>
		</div>
		<div class="row justify-content-center align-items-start">
			<?php if ( $the_query_2->have_posts() ) {
				while ( $the_query_2->have_posts() ) { $the_query_2->the_post(); ?>
					<div class="col-lg-3 col-md-6 col-sm-10 col-12 product-card-col">
						<?php
						$post_object = get_post( get_the_ID());

						setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

						wc_get_template_part( 'content', 'product' );
						?>
					</div>
				<?php }
			} else { ?>
				<div class="col-12 pt-5">
					<h4 class="block-title">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h4>
				</div>
				<div class="alert alert-info text-center mt-5">
					<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
