<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
$post_id = $product->get_id();
$post_link = $product->get_permalink();

$product_thumb = get_webp_img(get_post_thumbnail_id($post_id), 'large', '');
$product_gallery = $product->get_gallery_image_ids();
$product_gallery_images = [];
if($product_gallery){
	foreach ($product_gallery as $_item){
		$product_gallery_images[] = get_webp_img($_item, 'large', '');
	}
}
$fields = get_fields();
$tabs = [
		[
				'title' => 'תיאור המוצר',
				'content' => get_the_content(),
		],
		[
				'title' => 'משלוחים והחזרות',
				'content' => $fields['pro_delivery'] ?? opt('pro_delivery'),
		],
		[
				'title' => 'סוג חומר',
				'content' => $fields['pro_material'] ?? opt('pro_material'),
		]
];
$related_products = isset($fields['same_products']) && $fields['same_products'] ? $fields['same_products'] : '';

?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( '', $product ); ?>>
	<section class="mt-4">
		<div class="container">
			<div class="row">
				<div class="col-12">
				</div>
			</div>
		</div>
	</section>

	<section class="product-summary mb-5">
		<div class="container">
			<div class="row mobile-title-row">
				<div class="col-12">
					<h1 class="block-title"><?= $product->get_title(); ?></h1>
				</div>
			</div>
			<div class="row align-items-start justify-content-between">
				<div class="col-lg-6 col-12 col-xxl-5">
					<?php do_action('woocommerce_single_title_custom');
					/**
					 * Hook: woocommerce_single_product_summary.
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 * @hooked WC_Structured_Data::generate_product_data() - 60
					 */
					do_action( 'woocommerce_single_product_summary' );
					if ($tabs) : ?>
						<div id="accordion-product" class="product-info-acc <?php echo !($product->is_type('variable')) ? 'order-5' : ''; ?>">
							<?php foreach ($tabs as $number => $info_item) : ?>
								<div class="card">
									<div class="prod-desc-header" id="heading_<?= $number; ?>">
										<button class="btn accordion-button-wrap" data-toggle="collapse"
												data-target="#contactInfo<?= $number; ?>"
												aria-expanded="false" aria-controls="collapseOne">
											<span class="product-info-title"><?= $info_item['title']; ?></span>
											<span class="accordion-symbol <?= ($number === 0) ? 'show-icon' : ''; ?>">
												<img src="<?= ICONS ?>arrow-down.png" alt="read-more">
											</span>
										</button>
										<div id="contactInfo<?= $number; ?>" class="collapse
											<?php echo $number === 0 ? 'show' : ''; ?>"
											 aria-labelledby="heading_<?= $number; ?>" data-parent="#accordion-product">
											<div class="slider-output post-text-output">
												<?= $info_item['content']; ?>
											</div>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						</div>
					<?php endif; ?>
					<div class="socials-share mb-5">
					<span class="share-text">
						 שתף
					</span>
						<!--	WHATSAPP-->
						<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>" class="social-share-link">
							<img src="<?= ICONS ?>share-whatsapp.png">
						</a>
						<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
						   class="social-share-link">
							<img src="<?= ICONS ?>share-facebook.png">
						</a>
						<!--	MAIL-->
						<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
						   class="social-share-link">
							<img src="<?= ICONS ?>share-mail.png">
						</a>
					</div>
				</div>
				<div class="col-lg-6 col-12 mt-lg-0 mt-4 product-gallery-col">
					<div class="product-gallery w-100 arrows-slider arrows-slider-gallery">
						<div class="gallery-slider w-100" id="gallery-slider" dir="rtl">
							<?php if(has_post_thumbnail($post_id)): ?>
								<div class="slider-item slider-big-item">
									<?php echo do_shortcode( '[yith_wcwl_add_to_wishlist product_id="'.$product->get_id().'"]' ) ?>
									<?= $product_thumb ?>
								</div>
							<?php endif; ?>
							<?php foreach ($product_gallery_images as $img): ?>
								<div class="slider-item slider-big-item">
									<?php echo do_shortcode( '[yith_wcwl_add_to_wishlist product_id="'.$product->get_id().'"]' ) ?>
									<?= $img ?>
								</div>
							<?php endforeach; ?>
						</div>
						<?php if ($product_gallery_images) : ?>
							<div class="thumbs" id="gallery-thumbs" dir="rtl">
								<?php if(has_post_thumbnail($post_id)): ?>
									<div class="slider-item thumb-item">
										<?= $product_thumb ?>
									</div>
								<?php endif; ?>
								<?php foreach ($product_gallery_images as $img): ?>
									<div class="slider-item thumb-item">
										<?= $img ?>
									</div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php if($related_products): ?>
				<div class="col-12 related-products-col products-output px-md-0">
					<div class="d-flex flex-column justify-content-start align-items-center">
						<h4 class="base-title">
							<?= $fields['same_prod_title'] ? $fields['same_prod_title'] : 'המוצרים אצלנו בגורדון לרכישה מיידית'; ?>
						</h4>
					</div>
					<div class="related-slider">
						<?php

						echo get_template_part('views/partials/product', 'slider', [
								'products' => $related_products,
						]);

						?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>
<?php if (isset($fields['single_slider_seo']) && $fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider',
			[
					'content' => $fields['single_slider_seo'],
					'img' => $fields['slider_img'],
			]);
} ?>
<?php do_action( 'woocommerce_after_single_product' ); ?>
