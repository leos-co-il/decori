<?php
/**
 * Sidebar
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/sidebar.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_sidebar( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
global $wp_query;
$query_cat = [];

$cats = get_terms( [
	'taxonomy' => 'product_cat',
	'hide_empty' => false,
	'parent' => 0
] );
$query_curr = get_queried_object();
?>

<div class="accordion sidebar-block" id="accordion-cats">
	<div class="form-check-wrap">
		<?php foreach($cats as $number => $parent_term) : ?>
			<?php $term_children = get_term_children($parent_term->term_id, 'product_cat'); ?>
			<div class="card">
				<div class="card-header">
					<a class="prod-cat-title" href="<?= get_category_link($parent_term); ?>">
						<?= $parent_term->name; ?>
					</a>
					<?php if (!empty($term_children)) : ?>
						<button class="btn btn-link accordion-trigger" type="button">
						</button>
					<?php endif; ?>
				</div>
				<div id="collapse-<?= $number; ?>" class="collapse body-acc">
					<div class="card-body">
						<?php if ($term_children) : ?>
							<ul class="box-list">
								<?php foreach ($term_children as $x => $child) : $_data_item = get_term_by( 'id', $child, 'product_cat' ); ?>
									<li data-title="<?= $_data_item->name ?>" class=" check-li">
										<a href="<?= get_term_link($_data_item); ?>"><?= $_data_item->name ?></a>
									</li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
