<?php

the_post();
get_header();
$fields = get_fields();
$postId = get_the_ID();
$type = get_post_type();
$post_link = get_the_permalink();
$img = has_post_thumbnail() ? postThumb() : '';
?>
<article class="page-body post-page-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row post-title-mobile">
			<div class="col-12">
				<h1 class="block-title text-right mb-3"><?php the_title(); ?></h1>
			</div>
		</div>
		<div class="row justify-content-between">
			<div class="<?= $img ? 'col-lg-6 col-12' : 'col-12'; ?> post-content-col">
				<h1 class="block-title post-title-main text-right mb-3"><?php the_title(); ?></h1>
				<div class="base-output">
					<?php the_content(); ?>
				</div>
				<div class="socials-share">
					<span class="share-text"><?= esc_html__('שתף', 'leos'); ?></span>
					<!--	WHATSAPP-->
					<a href="https://api.whatsapp.com/send?text=<?php the_title(); echo $post_link; ?>" class="social-share-link">
						<img src="<?= ICONS ?>share-whatsapp.png" alt="whatsapp">
					</a>
					<a href="https://www.facebook.com/sharer/sharer.php?u=#<?= $post_link; ?>" target="_blank"
					   class="social-share-link">
						<img src="<?= ICONS ?>share-facebook.png" alt="facebook">
					</a>
					<!--	MAIL-->
					<a href="mailto:?subject=&body=<?= $post_link; ?>" target="_blank"
					   class="social-share-link">
						<img src="<?= ICONS ?>share-mail.png" alt="mail">
					</a>
				</div>
			</div>
			<?php if ($img) : ?>
				<div class="col-xl-5 col-lg-6 col-12 slider-img-col post-img-col position-relative align-items-start">
					<div class="slider-img-wrap post-form-col">
						<img src="<?= $img; ?>" alt="image">
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</article>
<?php
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
$samePosts = get_posts([
	'posts_per_page' => 4,
	'post_type' => 'post',
	'post__not_in' => array($postId),
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $post_terms,
		],
	],
]);
if ($samePosts === NULL) {
	$samePosts = get_posts([
		'posts_per_page' => 4,
		'orderby' => 'rand',
		'post_type' => $type,
		'post__not_in' => array($postId),
	]);
}
if ($samePosts) : ?>
	<section class="posts-block posts-block-same mt-5">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<h2 class="block-title">
						<?= $fields['same_title'] ? $fields['same_title'] : esc_html__('כתבות נוספות בתחום', 'leos');?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($samePosts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
if ($fields['single_slider_seo']) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $fields['single_slider_seo'],
		'img' => $fields['slider_img'],
	]);
}
get_footer(); ?>
