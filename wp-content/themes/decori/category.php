<?php
get_header();
$query = get_queried_object();
$posts = new WP_Query([
	'posts_per_page' => 8,
	'post_type' => 'post',
	'suppress_filters' => false,
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
$published_posts = new WP_Query([
	'posts_per_page' => -1,
	'post_type' => 'post',
	'suppress_filters' => false,
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
?>
<article class="page-body blog-body">
	<?php get_template_part('views/partials/repeat', 'breadcrumbs'); ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="base-output text-center mb-4">
					<h1><?= $query->name; ?></h1>
					<?= category_description(); ?>
				</div>
			</div>
		</div>
		<?php if ($posts->have_posts()) : ?>
			<div class="row align-items-stretch put-here-posts justify-content-center">
				<?php foreach ($posts->posts as $post) {
					get_template_part('views/partials/card', 'post',
						[
							'post' => $post,
						]);
				} ?>
			</div>
		<?php endif;
		if ($published_posts->have_posts() && (($num = count($published_posts->posts)) > 8)) : ?>
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="more-link regular-link load-more-posts" data-type="post"
						 data-count="<?= $num; ?>" data-term="<?= $query->term_id; ?>" data-term_name="category">
						<?= esc_html__('טען עוד..', 'leos'); ?>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
</article>
<?php
if ($seo = get_field('single_slider_seo', $query)) {
	get_template_part('views/partials/content', 'slider', [
		'content' => $seo,
		'img' => get_field('slider_img', $query),
	]);
}
get_footer(); ?>

