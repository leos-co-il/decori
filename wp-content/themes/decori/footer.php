<?php

$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
?>

<footer class="position-relative">
	<a id="go-top">
		<img src="<?= ICONS ?>to-top.png">
		<h5 class="to-top-text"> למעלה</h5>
	</a>
	<div class="foo-benefits">
		<?php get_template_part('views/partials/repeat', 'benefits'); ?>
	</div>
	<div class="newsletter-block">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="tnp tnp-subscription">
						<form method="post" action="<?= get_site_url(); ?>/?na=s">
							<div class="row align-items-center">
								<div class="col-lg-6 mb-lg-0 mb-4">
									<div class="row align-items-center justify-content-sm-start justify-content-center">
										<div class="col-sm-auto col-3">
											<div class="icon-newsletter">
												<?= svg_simple(ICONS.'newsteller.svg'); ?>
											</div>
										</div>
										<div class="col-sm col-12">
											<?php if ($title_foo = opt('foo_form_title')) : ?>
												<h3 class="foo-form-title"><?= $title_foo; ?></h3>
											<?php endif; ?>
											<div class="tnp-field tnp-privacy-field"><label>
													<input type="checkbox" name="ny" required class="tnp-privacy">  דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולורס מונפרד אדנדום </label></div>
										</div>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="tnp-field-wrap">
										<input type="hidden" name="nlang" value="">
										<div class="tnp-field tnp-field-email">
											<input class="tnp-email" type="email" name="ne" id="tnp-1" value="" required placeholder="הקלד את כתובת המייל">
										</div>
										<div class="tnp-field tnp-field-button">
											<input class="tnp-submit" type="submit" value="הרשמה >" >
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($foo_gallery = opt('foo_gallery')) : ?>
		<div class="foo-gallery-slider" dir="rtl">
			<?php foreach ($foo_gallery as $img) : ?>
				<div class="sl-foo-gal">
					<a class="foo-gal-img" href="<?= $img['url']; ?>" data-lightbox="gallery-footer">
						<img src="<?= $img['url']; ?>" alt="gallery-img">
					</a>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
	<div class="footer-main">
		<div class="container-fluid footer-container-menu">
			<div class="row justify-content-center">
				<div class="col-sm-11 col-12">
					<div class="row justify-content-between align-items-start">
						<div class="col-xl-1 col-md-4 col-sm-6 col-12 foo-menu">
							<h3 class="foo-title">אודותינו</h3>
							<div class="menu-border-top">
								<?php getMenu('about-footer-menu', '1'); ?>
							</div>
						</div>
						<div class="col-xl-2 col-md-4 col-sm-6 col-12 foo-menu">
							<h3 class="foo-title">תקנונים</h3>
							<div class="menu-border-top">
								<?php getMenu('regulations-footer-menu', '1'); ?>
							</div>
						</div>
						<div class="col-xl-2 col-md-4 col-sm-6 col-12 foo-menu">
							<h3 class="foo-title">המוצרים שלנו</h3>
							<div class="menu-border-top">
								<?php getMenu('products-footer-menu', '1'); ?>
							</div>
						</div>
						<div class="col-xl-2 col-md-4 col-sm-6 col-12 foo-menu">
							<h3 class="foo-title">צרו קשר</h3>
							<div class="menu-border-top">
								<?php getMenu('contacts-footer-menu', '1'); ?>
							</div>
						</div>
						<div class="col-xl-2 col-md-4 col-sm-6 col-12 foo-menu contacts-footer-menu">
							<h3 class="foo-title">שרות לקוחות</h3>
							<div class="menu-border-top">
								<ul class="contact-list d-flex flex-column">
									<?php if ($tel) : ?>
										<li>
											<a href="tel:<?= $tel; ?>" class="contact-info-footer">
												טלפון:<?= $tel; ?>
											</a>
										</li>
									<?php endif; ?>
									<?php if ($address) : ?>
										<li>
											<a href="https://waze.com/ul?q=<?= $address; ?>"
											   class="contact-info-footer" target="_blank">
												כתובת: <?= $address; ?>
											</a>
										</li>
									<?php endif; ?>
									<?php if ($mail) : ?>
										<li>
											<a href="mailto:<?= $mail; ?>" class="contact-info-footer">
												מייל: <?= $mail; ?>
											</a>
										</li>
									<?php endif; ?>
								</ul>
								<?php
								get_template_part('views/partials/repeat', 'socials');
								?>
							</div>
						</div>
						<?php if ($logo = opt('logo')) : ?>
							<div class="col-xl-3 col-md-4 col-sm-6 col-12 foo-menu foo-menu-logo d-flex flex-column">
								<h3 class="foo-title foo-title-opacity"></h3>
								<a href="/">
									<img src="<?= $logo['url']; ?>" alt="decori-logo">
								</a>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($pay_img = opt('payment_img')) : ?>
		<div class="pay-row py-3">
			<div class="container">
				<div class="row justify-content-between align-items-center">
					<div class="col-auto">
						<p class="pay-text">קנייה בטוחה</p>
					</div>
					<div class="col-auto pay-col">
						<p class="pay-text pl-3">*יש להעביר אפשריות קנייה</p>
						<img src="<?= $pay_img['url']; ?>">
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>
<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
